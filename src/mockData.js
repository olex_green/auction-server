function addMinutes(minutes) {
  return new Date(new Date().getTime() + minutes*60000);
}

const items = [
  {
    id: '1',
    title: 'Droid R2D2',
    price: 1500,
    user: '',
    endTime: addMinutes(5),
  },
  {
    id: '2',
    title: 'Charger for Droid R2D2',
    price: 200,
    user: '',
    endTime:  addMinutes(8),
  },
  {
    id: '3',
    title: 'Extra Solar Panel for Droid R2D2',
    price: 350,
    user: '',
    endTime:  addMinutes(10),
  },
  {
    id: '4',
    title: 'Droid C-3PO',
    price: 1400,
    user: '',
    endTime:  addMinutes(15),
  },
  {
    id: '5',
    title: 'Droid BB-8',
    price: 2300,
    user: '',
    endTime:  addMinutes(20),
  },
  {
    id: '6',
    title: 'Droid L3-37',
    price: 2850,
    user: '',
    endTime:  addMinutes(25),
  },
  {
    id: '7',
    title: 'Droid FX-7',
    price: 3400,
    user: '',
    endTime:  addMinutes(30),
  },
  {
    id: '8',
    title: 'Droid 3B6-RA-7',
    price: 1400,
    user: '',
    endTime:  addMinutes(35),
  },
  {
    id: '9',
    title: 'Droid GA-97',
    price: 3100,
    user: '',
    endTime:  addMinutes(40),
  },
  {
    id: '10',
    title: 'Droid R4-P17',
    price: 2100,
    user: '',
    endTime:  addMinutes(45),
  },
]

module.exports = {
  items,
};