const http = require('http');
const restify = require('restify');

const { items } = require('./mockData');

// Using the variable as global state - just as easiest solution - not production ready
// Better is using events for communication between SSE and API
let updatedItem = items[0];

const ACCESS_CONTROL_ALLOW_ORIGIN = 'http://localhost:3004';

http.createServer((request, response) => {
  response.writeHead(200, {
    Connection: 'keep-alive',
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Access-Control-Allow-Origin': ACCESS_CONTROL_ALLOW_ORIGIN,
  });

  setInterval(() => {
      response.write(
        `event: update\ndata:${JSON.stringify(updatedItem)}`
      );
      response.write('\n\n');
    }, 1000);
}).listen(8004);


const server = restify.createServer();
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.gzipResponse());

server.get('/items', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', ACCESS_CONTROL_ALLOW_ORIGIN);
  res.send(200, items);
  return next();
});

server.post('/bid', (req, res, next) => {
  res.header('Access-Control-Allow-Origin', ACCESS_CONTROL_ALLOW_ORIGIN);
  const {
    id,
    user,
    bid,
  } = req.body;

  const index = items.findIndex(item => item.id === id);

  if (index > -1) {
    items[index].price += +bid;
    items[index].user = user;
  
    updatedItem = {
      id,
      user,
      price: items[index].price,
    };
    res.send(200);
  } else {
    res.send(501);
  }

  
  return next();
});

server.listen(8003, () => {
  console.log('%s listening at %s', server.name, server.url);
});